#include <iostream>
#include "queue.h"

void initQueue(queue* q, unsigned int size)
{
	q->maxSize = size;
	q->amount = 0;
	q->queueAr = new unsigned int[size];
}
void cleanQueue(queue* q)
{
	delete[] q->queueAr;
}
void enqueue(queue* q, unsigned int newValue)
{
	q->queueAr[q->queueAr] = newValue;
	q->amount++;
}
int dequeue(queue* q)
{
	int ans = 0;
	if (q->amount == 0)
	{
		ans = -1;
	}
	else
	{
		ans = q->queueAr[0];
		for (int i = 1; 1 < q->amount; i++)
		{
			q->queueAr[i - 1] = q->queueAr[i];
		}
		q->queueAr[q->amount] = NULL;
		q->amount--;

	}
	return ans;
}








